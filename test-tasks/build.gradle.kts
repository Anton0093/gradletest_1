/*
 * This file was generated by the Gradle 'init' task.
 *
 * This generated file contains a sample Kotlin application project to get you started.
 * For more details take a look at the 'Building Java & JVM projects' chapter in the Gradle
 * User Manual available at https://docs.gradle.org/7.4.1/userguide/building_java_projects.html
 */

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.5.31"
    `java-gradle-plugin`
    application
//    id("org.anton.myTestPlugin")
}

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()
}

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    // Use the Kotlin JDK 8 standard library.
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    // This dependency is used by the application.
    implementation("com.google.guava:guava:30.1.1-jre")

    // Use the Kotlin test library.
    testImplementation("org.jetbrains.kotlin:kotlin-test")

    // Use the Kotlin JUnit integration.
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

gradlePlugin {
    plugins {
        create("simplePlugin") {
            id = "org.example.greeting"
            implementationClass = "org.example.GreetingPlugin"
        }
    }
}

application {
    // Define the main class for the application.
    mainClass.set("gradletest_1.AppKt")
}

abstract class GreetingTask : DefaultTask() {
    @TaskAction
    fun processed() {
        println("task is processed")
    }
}

val myTestTask_v1: TaskProvider<GreetingTask> = tasks.register<GreetingTask>("myTestTask_v1") {
    println("Initialization phase...")

    doFirst {
        println("start task")
    }

    doLast {
        println("end task")
    }
}

val myTestTask_v2: TaskProvider<DefaultTask> = tasks.register<DefaultTask>("myTestTask_v2") {
    println("Initialization phase...")

    doFirst {
        println("start task")
    }

    actions.add { println("task is processed") }

    doLast {
        println("end task")
    }
}

val myTestTask_v3: TaskProvider<DefaultTask> = tasks.register<DefaultTask>("myTestTask_v3") {
    println("Initialization phase...")

    doFirst {
        println("start task")
    }

    doLast {
        println("task is processed")
    }

    doLast {
        println("end task")
    }
}

// таска для создания мок файла с версией проекта
val createTextFile: TaskProvider<DefaultTask> = tasks.register<DefaultTask>("createTextFile") {
    outputs.file("project-version.txt")

    doFirst {
        val outputFile = outputs.files.singleFile
        outputFile.writeText("1.0.0")
    }
}

val incrementAppVersion: TaskProvider<DefaultTask> =
    tasks.register<DefaultTask>("incrementAppVersion") {
//        отложенная конфигурация, маппим один Provider в другой (из статьи хабра)
        inputs.file(
            createTextFile.map { it.outputs.files.singleFile }
        )

        doFirst {
            val inputFile = inputs.files.singleFile
            //  из 1.0.0 -> в [1, 0, 0]
            val strVersion = inputFile.readText().split(".").toTypedArray()
            // из массива строк массив чисел
            val intVersion = strVersion.map { it.toInt() }.toTypedArray()
            // обновляем версию - 101 и собираем в 1.0.1
            val newVersion = updateVersion(intVersion).joinToString(".", transform = Int::toString)
            // записываем файл
            inputFile.writeText(newVersion)
        }
    }

class UpdateVersionPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        project.task("incrementAppVersion")
    }
}

apply<UpdateVersionPlugin>()

fun updateVersion(digits: Array<Int>): Array<Int> {
    for (i in digits.size - 1 downTo 0) {
        digits[i] += 1
        if (digits[i] <= 9) return digits
        digits[i] = 0
    }
    val arr = Array(digits.size + 1) { 0 }
    arr[0] = 1

    return arr
}
